import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS }    from '@angular/common/http';

import { MaterialModule } from './material/material.module';

import { CategoryComponent } from './components/product/category/category.component';
import { ProductListComponent } from './components/product/product-list/product-list.component';
import { ProductInfoComponent } from './components/product/product-info/product-info.component';
import { AddProductComponent } from './components/product/add-product/add-product.component';

import { LoginComponent } from './components/user/login/login.component';
import { CartComponent } from './components/user/cart/cart.component';
import { OrderHistoryComponent } from './components/user/order-history/order-history.component';
import { UserInfoComponent } from './components/user/user-info/user-info.component';
import { SignupComponent } from './components/user/signup/signup.component';
import { SalesReportComponent } from './components/report/sales-report/sales-report.component';
import { HeaderComponent } from './components/shared/header/header.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { PageNotFoundComponent } from './components/shared/page-not-found/page-not-found.component';
import { ErrorComponent } from './components/shared/error/error.component';
import { ErrorEnterceptor } from './interceptors/error.interceptor';
import { ConfirmComponent } from './components/shared/confirm/confirm.component';
import { HttpRequestInterceptor } from './interceptors/http.interceptor';


@NgModule({
  declarations: [
    AppComponent,
    CategoryComponent,
    ProductListComponent,
    ProductInfoComponent,
    AddProductComponent,
    LoginComponent,
    CartComponent,
    OrderHistoryComponent,
    UserInfoComponent,
    SignupComponent,
    SalesReportComponent,

    HeaderComponent,
    FooterComponent,
    PageNotFoundComponent,
    ErrorComponent,
    ConfirmComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: ErrorEnterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: HttpRequestInterceptor, multi: true}
  ],
  entryComponents: [ErrorComponent, ConfirmComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
