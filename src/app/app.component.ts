import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from './components/user/user.service';
import { Subscription } from 'rxjs';
import { ProductService } from './components/product/product.service';
import { Category } from './components/product/category';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  constructor(private userService: UserService){}

  ngOnInit() {
    this.userService.updateTimer();
    this.userService.updateViewSize();
  }
}
