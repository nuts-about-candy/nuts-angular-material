import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/user/login/login.component';
import { SignupComponent } from './components/user/signup/signup.component';
import { ProductListComponent } from './components/product/product-list/product-list.component';
import { ProductInfoComponent } from './components/product/product-info/product-info.component';
import { PageNotFoundComponent } from './components/shared/page-not-found/page-not-found.component';
import { CartComponent } from './components/user/cart/cart.component';
import { OrderHistoryComponent } from './components/user/order-history/order-history.component';
import { UserInfoComponent } from './components/user/user-info/user-info.component';
import { CategoryComponent } from './components/product/category/category.component';
import { UserService } from './components/user/user.service';


const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'signup',
    component: SignupComponent
  },
  {
    path: 'profile',
    component: UserInfoComponent,
    canActivate : [UserService]
  },
  {
    path: 'cart',
    component: CartComponent
  },
  {
    path: 'order-history',
    component: OrderHistoryComponent,
    canActivate : [UserService]
  },
  {
    path: 'products',
    component: CategoryComponent
  },
  {
    path: 'products/view/:id',
    component: ProductInfoComponent
  },
  { path: '',
    redirectTo: '/products',
    pathMatch: 'full'
  },
  { path: '**',
    component: PageNotFoundComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
