import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';

@Injectable()
export class HttpRequestInterceptor implements HttpInterceptor{

  constructor(){}

  intercept(request: HttpRequest<unknown>, next: HttpHandler) {
    const token = localStorage.getItem("token");

    if(token)
      request = request.clone(
        { headers: request.headers.set('Authorization', token) }
      );
    return next.handle(request);
  }
}
