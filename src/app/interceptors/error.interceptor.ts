import { HttpInterceptor, HttpRequest, HttpHandler, HttpErrorResponse } from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ErrorComponent } from '../components/shared/error/error.component';

@Injectable()
export class ErrorEnterceptor implements HttpInterceptor{

  constructor(private dialog: MatDialog){}

  intercept(request: HttpRequest<unknown>, next: HttpHandler) {

    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {

        if(error.status == 0) {
          this.dialog.open(ErrorComponent, {data: {message:'Connection error.'}});
        }
        let message = "An unknown error occured.";
        if(error.error.message){
          message = error.error.message;
          this.dialog.open(ErrorComponent, {data: {message}});
        }
        return throwError(error);
      })
    );
  }
}
