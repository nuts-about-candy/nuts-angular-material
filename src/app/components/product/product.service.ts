import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Category } from './category';
import { Product } from './product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private productEndpoint = "http://localhost/api/inventory/products";
  private categoryEndpoint = "http://localhost/api/inventory/categories";


  private categoriesObservable = new Subject<
    {categories?: Category[]}>();
  private productsObservable = new Subject<
    {products?: Product[], isLoading?: boolean, category?: string, totalCount?: number}>();


  constructor(private http: HttpClient){}

  getCategoriesObservable() {
    return this.categoriesObservable.asObservable();
  }
  getProductsObservable() {
    return this.productsObservable.asObservable();
  }


  getCategories() {
    this.http.get(this.categoryEndpoint)
      .subscribe((response : Category[]) => {
        this.categoriesObservable.next({categories: response})
      })
  }

  getAllProducts(pageSize: number, currentPage: number) {
    const queryParams = `?pageSize=${pageSize}&page=${currentPage}`;

    this.http.get<{products: Product[], count: number}>(this.productEndpoint + queryParams)
      .subscribe(response => {
        this.productsObservable.next({products: response.products, totalCount: response.count})
      })
  }

  getProductsByCategory(catId: number, category: string) {
    this.productsObservable.next({isLoading: true, category})
    this.http.get<{products: Product[], count: number}>(this.productEndpoint + "/category/" + catId)
      .subscribe(response => {
        this.productsObservable.next({products: response.products, totalCount: response.count})
      })
  }

  getProductById(prodId: number) {
    return this.http.get(this.productEndpoint + "/" + prodId);
  }


}
