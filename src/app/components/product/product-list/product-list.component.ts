import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../../user/user.service';
import { ProductService } from '../product.service';
import { Subscription } from 'rxjs';
import { Product } from '../product';
import { NgForm } from '@angular/forms';
import { PageEvent } from '@angular/material/paginator';
import { ProductSize } from '../productSize';
import { UserModel } from '../../user/user.model';
import { CartService } from '../../user/cart.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit, OnDestroy {

  constructor(private userService: UserService,
    private productService: ProductService,
    private cartService: CartService
    ) { }

  products: Product[];
  productsSubscription: Subscription;
  sideNavSubscription : Subscription;
  userSubscription : Subscription;

  user: UserModel;
  isLoading = false;
  totalCount: number;
  pageSize = 8;
  pageSizeOptions = [8, 16, 64];
  currentPage = 1;
  title = "All Products";
  sideNavToggle = true;

  ngOnInit(): void {
    this.isLoading = true;
    this.subscribeUser();
    this.subscribeProduct();
    this.subscribeSideNav();
    this.userService.getUserModel();
    this.productService.getCategories();
    this.cartService.getCartItems(this.user);
    this.productService.getAllProducts(this.pageSize, this.currentPage);
  }
  subscribeUser() {
    this.userSubscription = this.userService.getUserObservableListener()
      .subscribe(response => {
        if(response.user)
          this.user = response.user;
      });
  }
  subscribeSideNav() {
    this.sideNavSubscription = this.userService.getSideNavbservableListener()
      .subscribe(response => {
        this.sideNavToggle = response.toggle;
      });
  }
  subscribeProduct() {
    this.productsSubscription = this.productService.getProductsObservable()
        .subscribe(response => {
          if(response.products) {
            this.products = response.products;
            this.totalCount = response.totalCount;
            this.isLoading = false;
          }
          if(response.isLoading)
            this.isLoading = response.isLoading;

          if(response.category)
            this.title = response.category;
        });
  }
  addToCart(prodId: number, productSizes: ProductSize[], form:NgForm) {

    this.cartService.addToCart(prodId, productSizes, form, this.user);
  }

  onChangedPage(pageData: PageEvent) {
    this.isLoading = true;
    this.currentPage = pageData.pageIndex + 1;
    this.pageSize = pageData.pageSize;
    this.productService.getAllProducts(this.pageSize, this.currentPage);
  }

  viewAllProducts() {
    this.isLoading = true;
    this.title = "All Products";
    this.productService.getAllProducts(this.pageSize, this.currentPage);
  }

  toggleCategories() {
    this.userService.toggleCategories(!this.sideNavToggle);
  }

  ngOnDestroy() {
    this.productsSubscription.unsubscribe();
    this.userSubscription.unsubscribe();
    this.sideNavSubscription.unsubscribe();
  }
}
