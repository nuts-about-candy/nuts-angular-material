import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserModel } from '../../user/user.model';
import { ProductService } from '../product.service';
import { Product } from '../product';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { UserService } from '../../user/user.service';
import { Subscription } from 'rxjs';
import { CartService } from '../../user/cart.service';

@Component({
  selector: 'app-product-info',
  templateUrl: './product-info.component.html',
  styleUrls: ['./product-info.component.css']
})
export class ProductInfoComponent implements OnInit, OnDestroy {

  user: UserModel;
  product = {} as Product;
  userSubscription : Subscription;

  constructor(private productService: ProductService,
    private cartService: CartService,
    private route: ActivatedRoute,
    private userService: UserService) { }

  ngOnInit(): void {
    this.subscribeUser();
    this.userService.getUserModel();
    this.cartService.getCartItems(this.user);
    const prodId = this.route.snapshot.paramMap.get('id');
    this.productService.getProductById(+prodId)
      .subscribe((product: Product) => {
        this.product = product;
      });
  }
  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  subscribeUser() {
    this.userSubscription = this.userService.getUserObservableListener()
      .subscribe(response => {
        if(response.user)
          this.user = response.user;
      });
  }

  addToCart(prodId: number, productSizes:any, form: NgForm) {
    this.cartService.addToCart(prodId, productSizes, form, this.user);
  }
}
