import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProductService } from '../product.service';
import { Category } from '../category';
import { Subscription } from 'rxjs';
import { UserService } from '../../user/user.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit, OnDestroy {

  opened = true;
  categories: Category[];
  categoriesSubscription : Subscription;
  sideNavSubscription : Subscription;

  constructor(private productService: ProductService,
    private userService: UserService) { }

  ngOnInit(): void {
    this.sideNavSubscription = this.userService.getSideNavbservableListener()
      .subscribe(response => {
        this.opened = response.toggle;
      })
    this.categoriesSubscription = this.productService.getCategoriesObservable()
    .subscribe(response => {
      if(response.categories)
        this.categories = response.categories;
    });
  }
  ngOnDestroy() {
    this.categoriesSubscription.unsubscribe();
    this.sideNavSubscription.unsubscribe();
  }

  getProductsByCategory(catId: number, category: string) {
    this.productService.getProductsByCategory(catId, category);
  }
}
