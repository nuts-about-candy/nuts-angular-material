import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserModel } from '../../user/user.model';
import { UserService } from '../../user/user.service';
import { Subscription } from 'rxjs';
import { ProductService } from '../../product/product.service';
import { CartService } from '../../user/cart.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  amount = 0;
  userModel = {} as UserModel;
  userSubscription: Subscription;
  navViewSubscription: Subscription;
  cartItemSubscription: Subscription;

  isLoggedIn = false;
  name = "";
  cartItemCount = 0;
  isXSmall = false;
  toggleNav = false;
  toggleIcon = "keyboard_arrow_down";
  constructor(private userService: UserService,
    private productService: ProductService,
    private cartService: CartService) { }

  ngOnInit(): void {
    this.navViewSubscription = this.userService.getNavViewObservableListener()
      .subscribe(result => {
        this.isXSmall = result;
      });

    this.userSubscription = this.userService.getUserObservableListener()
      .subscribe(response => {
        this.isLoggedIn = response.status;
        if(response.user){
          this.userModel = response.user;
          this.name = `${this.userModel.firstName} ${this.userModel.lastName}`;
        }
      });

      this.cartItemSubscription = this.cartService.getCartItemObservable()
        .subscribe(response => {
          this.cartItemCount = response.count;
        })
  }

  navToggler() {
    this.toggleNav = !this.toggleNav;
    if(this.toggleNav)
      this.toggleIcon = "keyboard_arrow_up";
    else {
      this.toggleIcon = "keyboard_arrow_down";
    }
  }
  ngOnDestroy(){
    this.userSubscription.unsubscribe();
    this.navViewSubscription.unsubscribe();
    this.cartItemSubscription.unsubscribe();
  }
  onLogout() {
    this.userService.logout();
  }

}
