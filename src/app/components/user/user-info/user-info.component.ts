import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../user.service';
import { Subscription } from 'rxjs';
import { CartService } from '../cart.service';
import { UserModel } from '../user.model';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit, OnDestroy {

  userSubscription : Subscription;
  user: UserModel;

  constructor(private userService: UserService,
    private cartService: CartService) { }

  ngOnInit(): void {
    this.userSubscriber();

    this.userService.getUserModel();
    this.cartService.getCartItems(this.user);
  }

  userSubscriber() {
    this.userSubscription = this.userService.getUserObservableListener()
      .subscribe(response => {
        if(response.user)
          this.user = response.user;
      });
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

}
