import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../user.service';
import { UserModel } from '../user.model';
import { Subscription } from 'rxjs';
import { CartItem } from '../cartItem';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmComponent } from '../../shared/confirm/confirm.component';
import { Router } from '@angular/router';
import { CheckoutDetails } from '../checkoutDetails';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit, OnDestroy {

  user: UserModel;
  cartItems: CartItem[];
  count: number;
  summary= {} as CheckoutDetails;

  userSubscription : Subscription;
  cartItemSubscription : Subscription;
  isLoading = false;

  constructor(private userService: UserService,
    private cartService: CartService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private router: Router) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.userSubscriber();
    this.cartItemSubscriber();

    this.userService.getUserModel();
    this.cartService.getCartItems(this.user);
  }
  ngOnDestroy() {
    this.userSubscription.unsubscribe();
    this.cartItemSubscription.unsubscribe();
  }
  userSubscriber() {
    this.userSubscription = this.userService.getUserObservableListener()
      .subscribe(response => {
        if(response.user)
          this.user = response.user;
      });
  }
  cartItemSubscriber() {
    this.cartItemSubscription = this.cartService.getCartItemObservable()
      .subscribe(response => {
        this.cartItems = response.cartItems;
        this.count = response.count;
        this.summary = response.summary;
        this.isLoading = false;
        if(this.count == 0)
          this.router.navigate(['/products']);
      });
  }

  minusQty(cartItem: CartItem) {
    this.cartService.updateQty(cartItem, 'minus', this.user);
  }
  addQty(cartItem: CartItem) {
    this.cartService.updateQty(cartItem, 'add', this.user);
  }

  removeItem(cartId: number, name: string, size: string) {
    const dialogRef = this.dialog.open(ConfirmComponent,
      {data: { message: `Are you sure you want to remove item '${name}', '${size}'?` }});

    dialogRef.afterClosed().subscribe(confirm => {
      if(confirm) {
        this.cartService.removeItemInCart(cartId, this.user);
        this.snackBar.open(`Item '${name}' has been removed to your cart.`, "Ok",{duration: 5000});
      }
    });
  }
}
