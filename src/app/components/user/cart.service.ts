import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { CartItem } from './cartItem';
import { CheckoutDetails } from './checkoutDetails';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserModel } from './user.model';
import { ConfirmComponent } from '../shared/confirm/confirm.component';
import { ProductSize } from '../product/productSize';
import { NgForm } from '@angular/forms';
import { ErrorComponent } from '../shared/error/error.component';
import { ProductService } from '../product/product.service';
import { Product } from '../product/product';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class CartService {

  private cartEndpoint = "http://localhost/api/cart";
  private LOCAL_STORAGE_CART = "cart";

  private cartItemObservable = new Subject<
    {count?: number, cartItems?: CartItem[], summary?: CheckoutDetails}>();

  constructor(private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private productService: ProductService,
    private http: HttpClient){}

  getCartItemObservable() {
    return this.cartItemObservable.asObservable();
  }

  getCartItems(user : UserModel){
    if(user) { // CALL TO DB
      this.http.get(this.cartEndpoint + "/" + user.id)
        .subscribe((response : CheckoutDetails) => {
          this.cartItemObservable.next({count: response.itemCount,
            cartItems: response.cartItems, summary: response});
        });
    }
    else { // GET FROM LOCAL STORAGE
      const summary = this.getCartItemsInStorage();
      if(summary) {
        this.cartItemObservable.next({count: summary.cartItems.length,
          cartItems: summary.cartItems, summary});
      }
      else
        this.cartItemObservable.next({count: 0});
    }
  }

  uploadCartItems(userId: number) {
    const summary = this.getCartItemsInStorage();
    if(summary)
      this.http.post(this.cartEndpoint + "/"+ userId +"/add-multiple", summary.cartItems)
        .subscribe(response => {
          localStorage.removeItem("cart");
        });
  }


  updateQty(cartItem: CartItem, action: string, user: UserModel){
    let item = {...cartItem};
    if(user) { // CALL REST RESOURCE
      this.updateOnlineCartItem(item, action, user)
    }
    else { // UPDATE LOCALSTORAGE
      this.updateLocalCartItem(item.id, action);
    }
  }

  private updateOnlineCartItem(cartItem : CartItem, action: string, user: UserModel) {

    const dialog = this.updateOrShowConfirmDialog(cartItem, action);

    if(dialog) {
      dialog.subscribe(confirm => {
        if(confirm){
          this.removeOnlineCartItem(cartItem.id, user);
          const name = cartItem.productDetails.name;
          this.snackBar.open(`Item '${name}' has been removed to your cart.`, "Ok",{duration: 3000});
        }
      });
    }
    else { // UDPATE CART ITEM AND GET ALL CART ITEMS
      this.http.put(this.cartEndpoint + "/" + cartItem.id, cartItem)
        .subscribe(response => {
          this.getCartItems(user);
        });
    }
  }

  private updateLocalCartItem(cartId: number, action: string) {
    const summary = this.getCartItemsInStorage();
    let cartItems = summary.cartItems;

    const filteredItem = this.getCartItemInStorage(item => item.id === cartId);
    if(filteredItem) {

      const dialog = this.updateOrShowConfirmDialog(filteredItem.item, action);
      if(dialog) {
        dialog.subscribe(confirm => {
          if(confirm){
            this.removeInLocalStorage(filteredItem.item.id);
            const name = filteredItem.item.productDetails.name;
          this.snackBar.open(`Item '${name}' has been removed to your cart.`, "Ok",{duration: 3000});
          }
        });
      }
      else {
        cartItems[filteredItem.index] = filteredItem.item;
        this.saveLocalCartItems(cartItems);
        this.snackBar.open(`Updated item's quantity.`, "Ok",{duration: 3000});
      }
    }
  }

  private updateOrShowConfirmDialog(cartItem: CartItem, action: string) {
    let operator = -1;
    if(action === 'add')
      operator = 1;

    cartItem.quantity += (operator);
    cartItem.totalWeight = cartItem.quantity * cartItem.productSize.weight;
    cartItem.subTotal = cartItem.quantity * cartItem.productSize.prize;

    if(cartItem.quantity === 0) {

      const dialogRef = this.dialog.open(ConfirmComponent,
        {data: { message: `Item will be remove to your cart.<br>Are you sure?` }});
      return dialogRef.afterClosed();
    }
  }

  addToCart(prodId: number, productSizes: ProductSize[], form:NgForm, user: UserModel) {
    let msg = "";
    if(!form.value.size)
      msg += "Please select Product size.<br>";
    if(!form.value.quantity)
      msg += "Please enter Quantity.";
    if(msg)
      this.dialog.open(ErrorComponent, {data: { message: msg }});

    if(msg === "") {
      let cartItem = {} as CartItem;
      cartItem.quantity = form.value.quantity;
      cartItem.size = form.value.size;
      cartItem.productId = prodId;

      const productSize = productSizes.filter(size => {
        return size.size == cartItem.size;
      })[0];
      cartItem.productSize = productSize;
      cartItem.totalWeight = cartItem.quantity * productSize.weight;
      cartItem.subTotal = cartItem.quantity * productSize.prize;

      this.productService.getProductById(prodId)
        .subscribe((product: Product) => {
          cartItem.productDetails = product;
          if((cartItem.quantity * productSize.weight) > product.stock)
            this.dialog.open(ErrorComponent, {data: { message: "Selected quantity exceeds current stock." }});
          else {
            this.snackBar.open(`Item '${product.name}' has been added to cart.`, "Ok", {duration: 5000});

            if(user) {  // ADD TO CART ONLINE
              cartItem.userId = user.id;
              this.http.post(this.cartEndpoint, cartItem)
                .subscribe(response => {
                  this.getCartItems(user);
                });
            }
            else {
              // ADD TO LOCALSTORAGE
              this.addToLocalStorage(cartItem);
            }
          }
        });
    }
  }

  removeItemInCart(cartId: number, user: UserModel) {
    if(user) { // REMOVE TO DB
      this.removeOnlineCartItem(cartId, user);
    }
    else { // REMOVE TO LOCAL STORAGE
      this.removeInLocalStorage(cartId);
    }
  }

  private removeOnlineCartItem(cartId: number, user: UserModel) {
    this.http.delete(this.cartEndpoint + "/" + cartId)
      .subscribe(response => {
        this.getCartItems(user);
      });
  }

  private removeInLocalStorage(cartiId: number) {
    const summary = this.getCartItemsInStorage();

    if(summary) {
      const cartItems = summary.cartItems;
      const filteredItems: CartItem[] = cartItems.filter(item => item.id !== cartiId);
      if(filteredItems.length > 0) {
        this.saveLocalCartItems(filteredItems);
      }
      else {
        localStorage.removeItem(this.LOCAL_STORAGE_CART);
        this.cartItemObservable.next({cartItems:[], count: 0});
      }
    }
  }
  private addToLocalStorage(item: CartItem) {
    const summary = this.getCartItemsInStorage();
    let newItem = true;
    let id = 1;
    let cartItems = [];
    if(summary) {
      cartItems = summary.cartItems;
      id = cartItems[cartItems.length - 1].id + 1; // GET THE LAST ID OF CART ITEM, PLUS 1
      let filteredItem = this.getCartItemInStorage(cartItem =>
            (cartItem.productDetails.id === item.productDetails.id &&
              cartItem.size === item.size));

      if(filteredItem) {
        newItem = false;
        filteredItem.item.quantity += item.quantity;
        cartItems[filteredItem.index] = filteredItem.item; // UDPATE SAME SIZE AND PRORUCT ID IN CART
      }
    }

    if(newItem) {
      item.id = id;
      cartItems.push(item);
    }
    this.saveLocalCartItems(cartItems);
  }

  private saveLocalCartItems(cartItems: CartItem[]) {

    localStorage.setItem(this.LOCAL_STORAGE_CART, JSON.stringify(cartItems));
    const summary = {
      subTotal: cartItems.reduce((total, item) => total + item.subTotal, 0),
      totalWeight: cartItems.reduce((total, item) => total + item.totalWeight, 0),
      cartItems:cartItems
    }
    this.cartItemObservable.next({count: cartItems.length, cartItems, summary});
  }

  private getCartItemsInStorage() {

    const items = localStorage.getItem(this.LOCAL_STORAGE_CART);
    if(items) {
      const cartItems: CartItem[] = JSON.parse(items);
      const summary: CheckoutDetails = {
        subTotal: cartItems.reduce((total, item) => total + item.subTotal, 0),
        totalWeight: cartItems.reduce((total, item) => total + item.totalWeight, 0),
        cartItems:cartItems
      }
      return summary;
    }
    return null;
  }

  private getCartItemInStorage(filterMethod) {

    const summary: CheckoutDetails = this.getCartItemsInStorage();
    if(summary && summary.cartItems.length > 0) {
      const filteredItems = summary.cartItems.filter(item => filterMethod(item));

      if(filteredItems.length > 0)
        return {item:filteredItems[0], index: summary.cartItems.indexOf(filteredItems[0])};
    }
  }
}
