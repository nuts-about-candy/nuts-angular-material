import { AuthModel } from './auth.model';

export class UserModel extends AuthModel{
  id?: number;
  lastName: string;
  firstName: string;
  access?: string;
}
