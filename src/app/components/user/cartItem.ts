import { Product } from '../product/product';
import { ProductSize } from '../product/productSize';

export class CartItem {
    id : number;
    userId : number;
    productId : number;
    size : string;
    quantity : number;

    productDetails : Product;
    productSize : ProductSize;

    totalWeight : number;
    subTotal : number;
}
