import { UserModel } from './user.model';
import { CartItem } from './cartItem';


export class CheckoutDetails {
    id? : number;
    user? : UserModel;
    cartItems? : CartItem[];
    deliveryFee? : number;
    subTotal? : number;
    totalWeight? : number;
    total? : number;
    discount? : number;
    itemCount? : number;
}
