import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserService } from '../user.service';
import { UserModel } from '../user.model';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  isLoading = false;
  userStatusSubs: Subscription;

  constructor(private userService: UserService,
    private router: Router,
    private cartService: CartService) { }

  ngOnInit(): void {
    this.userStatusSubs = this.userService.getUserObservableListener()
      .subscribe(authStatus => {
        this.isLoading = authStatus.status;
        if(authStatus.user)
          this.router.navigate(['/products']);
      });
      this.userService.getUserModel();
      this.cartService.getCartItems(undefined);
  }

  ngOnDestroy() {
    this.userStatusSubs.unsubscribe();
  }

  onSignUp(form : NgForm) {
    if(form.invalid) return;

    this.isLoading = true;
    const userModel: UserModel = {
      username: form.value.username,
      password: form.value.password,
      firstName: form.value.firstName,
      lastName: form.value.lastName,
    }
    this.userService.signup(userModel);
  }
}
