import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserService } from '../user.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  isLoading = false;
  userStatusSubs: Subscription;

  constructor(private userService: UserService,
    private router: Router,
    private cartService: CartService) { }

  ngOnInit(): void {
    this.userStatusSubs = this.userService.getUserObservableListener()
      .subscribe(authStatus => {
        this.isLoading = authStatus.status;
        if(authStatus.user)
          this.router.navigate(['/products']);
      });
      this.userService.getUserModel();
      this.cartService.getCartItems(undefined);
  }

  ngOnDestroy() {
    this.userStatusSubs.unsubscribe();
  }

  onLogin(form : NgForm) {
    if(form.invalid)
      return;

    this.isLoading = true;
    this.userService.login(form.value.username, form.value.password);
  }
}
