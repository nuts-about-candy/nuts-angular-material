import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../user.service';
import { Subscription } from 'rxjs';
import { UserModel } from '../user.model';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.css']
})
export class OrderHistoryComponent implements OnInit, OnDestroy {

  userSubscription : Subscription;
  user: UserModel;

  constructor(private userService: UserService,
    private cartService: CartService) { }

  ngOnInit(): void {
    this.userSubscriber();

    this.userService.getUserModel();
    this.cartService.getCartItems(this.user);
  }

  userSubscriber() {
    this.userSubscription = this.userService.getUserObservableListener()
      .subscribe(response => {
        if(response.user)
          this.user = response.user;
      });
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

}
