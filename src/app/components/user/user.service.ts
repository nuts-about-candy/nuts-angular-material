import { Injectable } from '@angular/core';
import { AuthModel } from './auth.model';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import * as jwt_decode from 'jwt-decode';
import { UserModel } from './user.model';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { ProductService } from '../product/product.service';
import { CartService } from './cart.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userEndpoint = "http://localhost/api/user";
  private navViewObservable = new Subject<boolean>();
  private sideNavObservable = new Subject<{toggle?:boolean}>();
  private userObservable = new Subject<
    {status?: boolean, user?: UserModel}>();

  userModel = {} as UserModel;
  tokenTimer: any;
  sideNavToggle = true;
  breakPoint: string;

  constructor(private http: HttpClient,
    private router: Router,
    public breakpointObserver: BreakpointObserver,
    private cartService : CartService){}

  getSideNavbservableListener() {
    return this.sideNavObservable.asObservable();
  }
  getNavViewObservableListener() {
    return this.navViewObservable.asObservable();
  }
  getUserObservableListener() {
    return this.userObservable.asObservable();
  }
  showNavView(show: boolean) {
    this.navViewObservable.next(show);
  }

  login(username: string, password: string){
    const authModel: AuthModel = {username, password};

    this.http.post(this.userEndpoint + "/login", authModel)
      .subscribe((response: any) => {
        const token = response.Authorization;
        const decodedToken = jwt_decode(token.replace("Bearer ", ""));
        this.userModel = decodedToken.sub;
        this.saveAuthData(decodedToken.sub, token, decodedToken.exp);
        this.userObservable.next({status: true, user: this.userModel})
        this.cartService.uploadCartItems(JSON.parse(decodedToken.sub).id);
        this.router.navigate(['/products']);
      }, error => this.userObservable.next({status: false}))
  }

  signup(userModel: UserModel) {
    this.http.post(this.userEndpoint + "/signup", userModel)
      .subscribe((response: any) => {
        this.router.navigate(['/login']);
      }, error => this.userObservable.next({status: false}));
  }

  updateTimer() {
    const expiration = this.getTokenExp();
    if(!expiration) return;

    const now = new Date();
    const expiresIn = expiration.getTime() - now.getTime();
    if(expiresIn > 0) {
      this.setAuthTimer(expiresIn);
    }
  }

  logout() {
    this.cartService.getCartItems(null);
    this.userObservable.next({status: false});
    clearTimeout(this.tokenTimer);
    this.clearAuthData();
    this.router.navigate(['/products']);
  }

  getUserModel() {
    const userModel = localStorage.getItem('userModel');
    if(userModel) {
      this.userModel = JSON.parse(userModel);
      this.userObservable.next({status: true, user: this.userModel})
    }
    //else this.logout();
  }

  getToken() {
    const token = localStorage.getItem('token');
    if(token) {
      return token;
    }
    //else this.logout();
  }

  private getTokenExp() {
    const expiration = localStorage.getItem('exp');
    if(expiration) return new Date(expiration);
  }

  private saveAuthData(userModel: string, token: string, exp: number) {
    const dateExp = new Date(new Date().getTime() + exp);
    localStorage.setItem('userModel', userModel);
    localStorage.setItem('token', token);
    localStorage.setItem('exp', dateExp.toISOString());
  }

  private clearAuthData() {
    localStorage.removeItem('userModel');
    localStorage.removeItem('token');
    localStorage.removeItem('exp');
  }

  private setAuthTimer(duration) {
    console.log('Setting timer: ' + duration);
    this.tokenTimer = setTimeout(() => {
      this.logout();
    }, duration);
  }

  canActivate() {
    const userModel = localStorage.getItem('userModel');
    if(!userModel) {
      this.logout();
      return false;
    }
    return true;
  }

  sideNavToggler(toggle: boolean){
    console.log(toggle);
    this.sideNavObservable.next({toggle: toggle});
    if(this.breakPoint === Breakpoints.Small ||
      this.breakPoint === Breakpoints.XSmall)
        this.showNavView(true);
  }

  toggleCategories(toggle) {
    this.sideNavObservable.next({toggle: toggle});
  }

  updateViewSize(){
    this.breakpointObserver.observe([
      Breakpoints.XSmall,
      Breakpoints.Small,
      Breakpoints.Medium,
      Breakpoints.Large,
      Breakpoints.XLarge
    ]).subscribe(result => {
      if (result.breakpoints[Breakpoints.XSmall]) {
        this.breakPoint = Breakpoints.XSmall;
        this.showNavView(true);
        this.sideNavObservable.next({toggle: false});
       }
       if (result.breakpoints[Breakpoints.Small]) {
        this.breakPoint = Breakpoints.Small;
        this.showNavView(false);
        this.sideNavObservable.next({toggle: true});
       }
       if (result.breakpoints[Breakpoints.Medium]) {
        this.breakPoint = Breakpoints.Medium;
        this.showNavView(false);
        this.sideNavObservable.next({toggle: true});
       }
       if (result.breakpoints[Breakpoints.Large]) {
        this.breakPoint = Breakpoints.Large;
        this.showNavView(false);
        this.sideNavObservable.next({toggle: true});
       }
       if (result.breakpoints[Breakpoints.XLarge]) {
        this.breakPoint = Breakpoints.XLarge;
        this.showNavView(false);
        this.sideNavObservable.next({toggle: true});
       }
    });
  }
}
